instll_zsh_and_dependencies_pkgs:
  pkg.installed:
    - pkgs:
      - zsh
      - git

oh_myzsh_repository:
  git.latest:
    - name: "{{ pillar['common']['zsh']['repo'] }}"
    - target: "/usr/local/share/oh-my-zsh"
    - force_clone: True
    - require:
      - pkg: instll_zsh_and_dependencies_pkgs

{% for user in pillar['common']['zsh']['users'] %}
{% if user == 'root' %}
backup_root_zshrc:
  cmd.run:
    - name: if [ -f /root/.zshrc ]; then mv /root/.zshrc{,.orig}; fi

zsh_custom_root:
  file.recurse:
    - name: /root/.ohmyzsh
    - source: salt://common/files/zsh/.ohmyzsh
    - user: root
    - group: root

zshrc_file_root:
  file.managed:
    - name: /root/.zshrc
    - source: salt://common/files/zsh/.zshrc
    - user: root
    - group: root
    - mode: 644

set_zsh_shell_root:
  user.present:
    - name: root
    - shell: /bin/zsh
    - require:
      - file: zsh_custom_root
      - file: zshrc_file_root

{% else %}
backup_{{ user }}_zshrc:
  cmd.run:
    - name: if [ -f /home/{{ user }}/.zshrc ]; then mv /home/{{ user }}/.zshrc{,.orig}; fi

zsh_custom_{{ user }}:
  file.recurse:
    - name: /home/{{ user }}/.ohmyzsh
    - source: salt://common/files/zsh/.ohmyzsh
    - user: {{ user }}
    - group: {{ user }}

zshrc_file_{{ user }}:
  file.managed:
    - name: /home/{{ user }}/.zshrc
    - source: salt://common/files/zsh/.zshrc
    - user: {{ user }}
    - group: {{ user }}
    - mode: 644

set_zsh_shell_{{ user }}:
  user.present:
    - name: {{ user }}
    - shell: /bin/zsh
    - require:
      - file: zsh_custom_{{ user }}
      - file: zshrc_file_{{ user }}
{% endif  %}
{% endfor %}

