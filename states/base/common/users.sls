{% for user, args in pillar['common']['users'].items() %}
create users {{user}}:
  user.present:
    - name: {{ user }}
{% if pillar['common_gpg']['users']['' ~ user ~ '']['password'] is defined %}
    - password: "{{ salt['cmd.run']('openssl passwd -1 ' ~ pillar['common_gpg']['users']['' ~ user ~'']['password']) }}"
{% endif %}
{% if 'auth_key' in args %}
add sshkeys user {{ user }}:
  ssh_auth.present:
    - user: {{user}}
    - enc: ssh-rsa
    - options:
      - no-port-forwarding
      - no-agent-forwarding
      - no-X11-forwarding
    - names:
      - {{ args['auth_key'] }}
{% endif %}
{% endfor %}
